package clock;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ClockTest {

	@Test
	void testClockModel() {
		fail("Not yet implemented");
	}

	@Test
	void testClockModelIntIntInt() {
		fail("Not yet implemented");
	}

	@Test
	void testSetTime() {
		ClockModel c = new ClockModel(5, 6, 7);
		assertEquals(5, c.getSeconds());
		assertEquals(6, c.getMinutes());
		assertEquals(7, c.getHours());
		
		c.setTime(6, 29, 44);
		assertEquals(6, c.getSeconds());
		assertEquals(29, c.getMinutes());
		assertEquals(44, c.getHours());
	}

	@Test
	void testSetSeconds() {
		fail("Not yet implemented");
	}

	@Test
	void testIncrementMinutes() {
		fail("Not yet implemented");
	}

}
